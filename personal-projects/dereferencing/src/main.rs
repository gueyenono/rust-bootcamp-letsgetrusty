fn main() {
    
    let mut x: Box<i32> = Box::new(1); // x points to 1 on the heap
    let a: i32 = *x; // a = 1 on the stack (x is dereferenced)
    println!("The value of x is {x}.");
    println!("The value of a is {a}.");

    *x += 1; // x now points to 2 on the heap
    println!("The value of x is now {x}");

    let r1: &i32 = &x; // r1 points to x on the stack (and x points to 2 now)
    println!("The value of r1 is {r1}");

    let b: i32 = **r1; // ** (2 stars), first one to point to x then second one to point to the heap value 2 but b is on the stack
    println!("The value of b is {b}.");

    let r2: &i32 = &*x; // *x dereferences x and the value is one the stack then & let r2 point to the heap value directly
    println!("The value of r2 is {r2}");

    let c: i32 = *r2; // dereferences r2 so its value is held by c on the stack
    println!("The value of c is {c}");

}
