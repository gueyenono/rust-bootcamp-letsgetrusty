// Main tasks
/* Ask user for conversion direction
    Let user enter temperature
    Convert to other unit */

use std::io;

fn main() {
    
    loop {

        let option: u8 = 'conversion_choice: loop {

            // User chooses option
            println!("");
            println!("Temperature conversation:");
            println!("1 - Celsius --> Fahrenheit");
            println!("2 - Fahrenheit --> Celsius");
            println!("");

            let mut option: String = String::new();

            io::stdin()
                .read_line(&mut option)
                .expect("Invalid input.");

            let option: u8 = option
                .trim()
                .parse()
                .expect("Invalid input.");

            if option == 1 || option == 2 {
                break 'conversion_choice option;
            } else {
                println!("");
                println!("Your two options are 1 and 2.");
                continue;
            }

        };

        // User enters temperature
        println!("");
        println!("Enter temperature:");
        let mut temp: String = String::new();

        io::stdin()
            .read_line(&mut temp)
            .expect("Invalid input.");

        let temp: f32 = temp
            .trim()
            .parse()
            .expect("You need to enter a valid temperature.");

        // Temperature conversion
        let results: (f32, &str) = if option == 1 {
            ((temp*1.8)+32.0, "F")
        } else {
            ((temp-32.0)/1.8, "C")
        };

        println!("The temperature is {}{}.", results.0, results.1);
        println!("");

        // Ask user if they want to continue
        println!("Would you like to keep going? (y/n)");

        let mut keep_going: String = String::new();

        io::stdin()
            .read_line(&mut keep_going)
            .expect("Invalid input");

        let keep_going: &str = keep_going.trim();

        if keep_going == "y" {
            continue; 
        }
        
        if keep_going == "n" {
            break; 
        } 

    }

}
