fn main() {
    // Declare constants
    const TRUE_LOVE: &str = "My true love sent to me";
    const DAY: [&str; 12] = [
        "A partridge in a pear tree",
        "Two turtle doves",
        "Three French hens",
        "Four calling birds",
        "Five golden rings",
        "Six geese a-laying",
        "Seven swans a-swimming",
        "Eight maids a-milking",
        "Nine ladies dancing",
        "Ten lords a-leaping",
        "Eleven pipers piping",
        "Twelve drummers drumming"
    ];

    const ORDER: [&str; 12] = [
        "first", "second", "third", "four",
        "fifth", "sixth", "seventh", "eighth",
        "ninth", "tenth", "eleventh", "twelfth"
    ];

    for day in 0..DAY.len() {

        println!("On the {} day of Christmas", ORDER[day]);
        println!("{TRUE_LOVE}");

        if day == 0 {
            println!("{}", DAY[0]);
        } else {
            for line in (0..=day).rev() {
                println!("{}", DAY[line]);
            } 
        }

        println!("");
    }
}
