use std::io; // For reading user input

fn main() {
    println!("Enter your year of birth?");

    // Create variable that will hold user input
    let mut birthyear: String = String::new();

    // Get user input
    io::stdin()
        .read_line(&mut birthyear)
        .expect("Invalid input.");

    // Convert user input to numeric
    let birthyear: u32 = match birthyear.trim().parse() {
        Ok(year) => year,
        Err(_) => 5, // Not sure how to handle this yet.
    };

    // Compute user's age
    let age: u32 = 2024 - birthyear;

    // Display user's age
    println!("You are {age} years old.");
}
