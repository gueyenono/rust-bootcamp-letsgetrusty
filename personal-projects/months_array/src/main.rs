use std::io; // For getting user input

fn main() {
    let months: [&str; 12] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    loop {
        println!("Enter the month index you want:");

        // Get user input
        let mut index: String = String::new();

        io::stdin()
            .read_line(&mut index)
            .expect("Invalid index");

        let index: usize = index.trim().parse().expect("Index was not a number.");

        println!("The month is {}.", months[index-1]);
    }

}
