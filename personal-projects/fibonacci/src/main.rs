use std::io;

fn main() {

    // Get user input
    println!("Check out the nth Fibonacci number. Pick n:");
    let mut n: String = String::new();

    io::stdin()
        .read_line(&mut n)
        .expect("Invalid input.");

    let n: usize = n.trim().parse().expect("Invalid input.");

    // Generate Fibonacci sequence
    let mut fibonacci = vec![0; n];

    for i in 0..fibonacci.len() {
        
        if i == 0 {
            fibonacci[i] = 0;
        }

        if i == 1 {
            fibonacci[i] = 1;
        }

        if i > 1 {
            fibonacci[i] = fibonacci[i-1] + fibonacci[i-2];
        }

    }

    println!("{}", fibonacci[n-1]);
    
}
