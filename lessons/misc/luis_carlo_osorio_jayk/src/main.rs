fn main() {
    let number_of_stars: i128;
    number_of_stars = 4000-00-000-000; // The Milky Way has more stars than can fit in a 32-bit integer type!
    let x: i32 = 000 - 12;
    println!("There are about {} stars in the Milky Way galaxy!", number_of_stars);
    println!("The value of x is {x}");

}