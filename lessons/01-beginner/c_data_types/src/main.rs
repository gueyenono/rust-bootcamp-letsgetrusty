fn main() {
    
    // ~~ SCALAR DATA TYPES ~~
    // They store a single value

    // Boolean
    let b1: bool = true;
    let b2: bool = false;
    println!("The value of b1 is {b1} and the value of b2 is {b2}.");

    // Unsigned integers (must be positive integers)
    let i1: u8 = 1;
    let i2: u16 = 1;
    let i3: u32 = 1;
    let i4: u64 = 1;
    let i5: u128 = 1;

    // Signed integers (can be positive or negative)
    let i6: i8 = -1;
    let i7: i16 = 1;
    let i8: i32 = -1;
    let i9: i64 = 1;
    let i10: i128 = -1;

    // Floating point numbers (decimal numbers)
    let f1: f32 = 1.0;
    let f2: f64 = 1.0;

    // Platform-specific integers (not used often)
    let p1: usize = 1;
    let p2: isize = 1;

    // Characters, &str, and Strings
    let c1: char = 'a'; // Single character
    let s1: &str = "GOD is good!"; // String slice (every string literal is a string slice)
    let s2: String = String::from("GOD is good!"); // String type


    // // Characters
    // let c1: char = 'a';
    // let c2: char = 'a';

    // // String
    // let s1: String = "Hello, world!".to_string();
    // let s2: String = "Hello, world!".to_string();


    // ~~ COMPOUND DATA TYPES ~~
    // They store multiple values

    // Arrays (must hold values of the same type)
    let a1: [i32; 5] = [1, 2, 3, 4, 5];
    let x: i32 = a1[4]; // Get last element from array a1

    // Tuples (may hold values of different types)
    let t1: (i32, i32, i32) = (1, 2, 3); // Tuple of 32-bit integers only
    let t2: (i32, f64, &str) = (20, 5.2, "GOD is great");

    let y1: i32 = t1.2; // Indexing into a tuple
    let (a, b, c) = t2; // Destructuring tuples

    let unit: () = (); // A unit type is an empty tuple

    // Type aliasing (new name for an existing type)
    type age = u8;
    let w: age = 51;

}
