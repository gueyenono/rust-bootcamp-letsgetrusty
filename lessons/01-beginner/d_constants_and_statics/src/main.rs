// Constants
// - Cannot be mutated (nor can be made mutable like default variables)
// - Must be assigned a constant value
// - Requires an explicit type annotation
// - Naming convention is screaming snake case
// - Can be declared in any scope including the global scope
const MAX_PLAYERS: U8 = 10;

// Static
// - Must be assigned a constant value
// - Requires an explicit type annotation
// - Naming convention is screaming snake case
// - Can be declared in any scope including the global scope
// - Can be marked as mutable. 
static mut SCHOOL_NAME: &str = "Louisiana Tech University"

// Static
// static 

 fn main() {

}
