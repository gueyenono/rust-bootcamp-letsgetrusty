fn main() {
    // if else
    let mut a: i32 = 5;

    if a > 5 {
        println!("Bigger than 5")
    } else if a > 3 {
        println!("Bigger than 3")
    } else {
        println!("Less than or equal to 3")
    }

    let b: i32 = if a > 5 {1} else {-1};
    println!("The value of b is {b}.");

    // loop
    'outer: loop {
        println!("In the outer loop!");

        'inner: loop {
            println!("In the inner loop!");
            break 'outer; // You can mention the name of the loop to break
        }

        // break; // Not needed because the loop is already broken
    }

    // while loop
    while a < 10 {
        println!("a is {a}.");
        a = a + 1;
    }

    // for loop
    let arr: [u8; 5] = [2, 9, 4, 7, 10];

    for el in arr {
        println!("Current element is {el}.")
    }
}
