fn main() {
    let z: u8 = my_function(32);
    println!("The function returned {z}.")
}

fn my_function(x: i32) -> u8 {
    println!("my_function is called with {x}.");
    let y: u8 = 5;
    y // Omit the semicolon for `y` to be the return value
}