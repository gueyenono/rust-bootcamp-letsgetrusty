fn main() {
    // Create a variable
    let a = 5; // Rust infers that `a` is an 32-bit integer
    let a2: i16 = 10; // Here we specify the type of our integer (16-bit)

    println!("The value of a is {a}");
    println!("The value of a2 is {a2}");

    // Mutability
    let b: i32 = 5; // Variables are immutable by default, we can't assign a new value;
    // b = 10; // This will not work!

    println!("The value of b is {b}");

    // Shadowing
    let c: i32 = 10;
    println!("The value of c is {c}");
    let c: i32 = 20; // This variable shadows (overwrites) the first one
    println!("The value of c is {c}");

    // Mutability modifies a variable. Shadowing creates a new variable.

    // Scope
    let d: i32 = 30;

    {
        println!("The value of d is {d}");
        let d: i32 = 40;
        println!("The value of d is {d}");

        let e: i32 = 100;
    }

    println!("The value of d is {d}");
    // println!("The value of e is {e}"); // e cannot be accessed by this!


}
