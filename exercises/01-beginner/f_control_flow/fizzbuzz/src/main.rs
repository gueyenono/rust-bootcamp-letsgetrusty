fn main() {
    
    let mut i: i32 = 1;
    while i <= 100 {

        if i % 15 == 0 {
            println!("Fizzbuzz");
        } else if i % 3 == 0 {
            println!("Fizz");
        } else if i % 5 == 0 {
            println!("Buzz");
        } else {
            println!("{i}");
        }

        i = i + 1;
    }

}
