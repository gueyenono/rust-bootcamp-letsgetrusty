fn main() {
    let numbers = [36, 25, 3, 49, 64, 16, 9];
    let prime = get_prime(numbers);
    println!("The first prime number is {prime}.")
}

fn get_prime(arr: [i32; 7]) -> i32 {
    
    // Loop through the array
    // Get out only if prime number is found
    let mut i = 0;
    'outer: loop {

        // Divide each array element by a number of divisors:
        let mut n = 2;
        'inner: loop {
            
            if arr[i] % n == 0 {
                if arr[i] == n {
                    break 'outer;
                } else {
                    break 'inner
                }
            } 

            n = n + 1;
        }

        // If loop gets to the end of the array with no solution --> break
        if i == (arr.len() - 1) {
            break 'outer;
        }

        i = i + 1;
    }

    arr[i] // Return prime number found in array

}