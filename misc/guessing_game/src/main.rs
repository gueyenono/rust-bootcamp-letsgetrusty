use rand::Rng; // For random number generator
use std::cmp::Ordering;
use std::io; // For reading user input

fn main() {
    println!("Guess the number!");

    let secret: i32 = rand::thread_rng().gen_range(1..=100);
    println!("The secret is {secret}.");

    loop {

        println!("Please input your guess:");

        let mut guess: String = String::new();

        // Assign user input to mutable reference of `guess`.
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read guess.");

        // Convert `guess` from String to i32
        let guess: i32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("Your guess was: {guess}.");

        match guess.cmp(&secret) {
            Ordering::Less => println!("Too low!"),
            Ordering::Greater => println!("Too high!"),
            Ordering::Equal => {
                println!("YOU GOT IT!");
                break;
            }

        }

        // if guess < secret {println!("Too low!");}
        // if guess > secret {println!("Too high!");}
        // if guess == secret {
        //     println!("YOU GOT IT!");
        //     break;
        // }

    }
}
